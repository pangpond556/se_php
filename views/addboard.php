<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

  textarea{
  height:400px;
  min-height:400px;
  max-height:400px;
  }
  </style>
</head>
<body>
<div class="container">
  <div class="p-3 mb-2 text-center"><br><h3>Creat Post</h3><br></div>
  <div class="row content well well-lg">

    <div class="text-left">
      <form class="form-horizontal">
        <div class="form-group">
          <label for="usr">Title</label>
          <input type="text" class="form-control" id="usr">
          <label for="exampleFormControlFile1">Image Title</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1">
          <label for="comment">Detail</label>
          <textarea class="form-control" rows="5" id="comment"></textarea>
          <label for="sel1">Tag</label>
          <select class="form-control" id="sel1">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
        </div>
      </form>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</div>


</body>
</html>
