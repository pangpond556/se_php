<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

  textarea{
  height:100px;
  min-height:100px;
  max-height:100px;
  }
  </style>

</head>
<body>
<div class="p-3 mb-2 text-center"><br><h1>Title</h1><br></div>
<div class="container ">
  <div class="well well-lg" style="position: relative">
    <h2>Detail</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    <div class="btn-group" style="position: absolute;top: 0px;right: 0;">
      <button type="button" class="btn btn-primary">Edit</button>
      <button type="button" class="btn btn-danger">Delete</button>

    </div>
  </div>

    <div ><br><h3>Comments</h3><br></div>
    <div class="well well-sm">
      <div ><br><h4>User name</h4><br></div>
      <div ><br>comment.....<br></div>
    </div>
    <div class="well well-sm">
      <div ><br><h4>User name</h4><br></div>
      <div ><br>comment.....<br></div>
    </div>
    <div class="well well-sm">
      <div ><br><h4>User name</h4><br></div>
      <div ><br>comment.....<br></div>
    </div>
    <hr>
    <div ><br><h3>Comment Now</h3><br></div>
    <form class="form-horizontal">
      <div class="form-group">
        <textarea class="form-control" rows="5" id="comment"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Commend</button>
    </form>
</div>


</body>
</html>
