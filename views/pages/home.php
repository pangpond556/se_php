<?php require_once('views/logo.php'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-7">
			<?php require_once('views/pages/home/followingnews.php'); ?>
		</div>
		<div class="col-md-5">
			<?php require_once('views/pages/home/incomingevents.php'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="HotNews" class="container-fluid">
				<ul class="nav nav-pills">
					<li class="active"><a data-toggle="tab" href="#hotnews">
						<strong>ข่าวประชาสัมพันธ์</strong>
					</a></li>
					<li><a data-toggle="tab" href="#student_news">
						<strong>สำหรับนักศึกษา</strong>
					</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="hotnews" class="tab-pane fade in active">
						<br>
						<?php for($i=0;$i<count($hotnews->hotnews);$i++): ?>
							<p><?php echo $hotnews->hotnews[$i]; ?></p>
						<?php endfor; ?>
					</div>
					<div id="student_news" class="tab-pane fade">
						<br>
						<p>test</p>
					</div>
				</div>			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<?php require_once('views/pages/home/library.php'); ?>
		</div>
		<div class="col-md-7">
			<?php require_once('views/pages/home/blog.php'); ?>
		</div>
	</div>
</div>

<style>
	#HotNews {
		background-color: white;
		border: 1px solid #e1e8ed;
		border-radius: 5px;
		height: 250px;
	}
</style>