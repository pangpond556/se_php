<style >
.btn-circle.btn-lg {
width: 50px;
height: 50px;
padding: 10px 16px;
font-size: 18px;
line-height: 1.33;
border-radius: 25px;
}
.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}

</style>
<div class="container">
  <!-- ปุ่มบวกอยู่นี้ -->
  <button type="button" class="btn btn-success btn-circle btn-lg" style="position:fixed;z-index:99;bottom:20px;right:20px">+</button>
  <div class="p-3 mb-2 text-center"><br><h3>Board</h3><br></div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="https://placehold.it/1200x400?text=IMAGE" alt="Image">
        <div class="carousel-caption">
          <h3>Sell $</h3>
          <p>Money Money.</p>
        </div>
      </div>

      <div class="item">
        <img src="https://placehold.it/1200x400?text=Another Image Maybe" alt="Image">
        <div class="carousel-caption">
          <h3>More Sell $</h3>
          <p>Lorem ipsum...</p>
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>

<br>

  <div class="row container">

    <div class="col-sm-10 text-left ">
      <div class="row">
        <div class="carousel slide col-md-2"  data-ride="carousel">

            <div class="carousel-inner ">
                <div class="item active">
                  <img class="img-fluid rounded" style="height:150px;width:150px;" src="https://www1.reg.cmu.ac.th/web/wp-content/uploads/2017/10/DSC_7334.jpg">
                </div>
          </div>

        </div>
        <div class="item col-sm-10">
      <h3>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วก่อนหน้านั้นเคยรู้อะไรผิดๆมาบ้าง</h1>
      <p>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วเคยรู้อะไรผิดๆมาบ้างครับ ของผมสมัยตอนอนุบาล 2 ผมเคยถามแม่ว่า เด็กเกิดมาได้ยังไง แม่เล่าให้ฟังว่า เด็กเกิดมาจากก้อนเลือด</p>
    </div>


    </div>
      <hr>
    <div class="row">
      <div class="carousel slide col-md-2"  data-ride="carousel">

          <div class="carousel-inner ">
              <div class="item active">
                <img class="img-fluid rounded" style="height:150px;width:150px;" src="https://www1.reg.cmu.ac.th/web/wp-content/uploads/2017/10/DSC_7334.jpg">
              </div>
        </div>

      </div>
      <div class="item col-sm-10">
    <h3>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วก่อนหน้านั้นเคยรู้อะไรผิดๆมาบ้าง</h1>
    <p>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วเคยรู้อะไรผิดๆมาบ้างครับ ของผมสมัยตอนอนุบาล 2 ผมเคยถามแม่ว่า เด็กเกิดมาได้ยังไง แม่เล่าให้ฟังว่า เด็กเกิดมาจากก้อนเลือด</p>
  </div>

  </div>
  <hr>
  <div class="row ">
    <div class="carousel slide col-md-2"  data-ride="carousel">

        <div class="carousel-inner ">
            <div class="item active">
              <img class="img-fluid rounded" style="height:150px;width:150px;" src="https://www1.reg.cmu.ac.th/web/wp-content/uploads/2017/10/DSC_7334.jpg">
            </div>
      </div>

    </div>
    <div class="item col-sm-10">
  <h3>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วก่อนหน้านั้นเคยรู้อะไรผิดๆมาบ้าง</h1>
  <p>ครั้งแรกที่คุณรู้ว่าเด็กเกิดมาได้ยังไงนั้น คุณรู้มาจากใคร รู้ได้ยังไงครับ แล้วเคยรู้อะไรผิดๆมาบ้างครับ ของผมสมัยตอนอนุบาล 2 ผมเคยถามแม่ว่า เด็กเกิดมาได้ยังไง แม่เล่าให้ฟังว่า เด็กเกิดมาจากก้อนเลือด</p>
</div>

</div>
<hr>
    </div>
    <p class="text-center ">Tag</p>
    <div class="col-sm-2 panel panel-default">
      <ul class="list-group">
        <li class="list-group-item">ทั่วไป<span class="badge">3</span></li>
        <li class="list-group-item">การเรียน<span class="badge">3</span></li>
        <li class="list-group-item">ความรัก<span class="badge">3</span></li>
        <li class="list-group-item">ทั่วไป<span class="badge">3</span></li>
        <li class="list-group-item">การเรียน<span class="badge">3</span></li>
        <li class="list-group-item">ความรัก<span class="badge">3</span></li>
        <li class="list-group-item">ทั่วไป<span class="badge">3</span></li>
        <li class="list-group-item">การเรียน<span class="badge">3</span></li>
        <li class="list-group-item">ความรัก<span class="badge">3</span></li>
      </ul>
    </div>
  </div>

</div>
