<div id="Library" class="container-fluid">
	<h4><strong>Library</strong></h4>
	<h5>หนังสือที่ยืม</h5>
	<table class="table">
		<thead>
			<th></th>
			<th>ชื่อหนังสือ</th>
			<th>วันที่ยืม</th>
			<th>วันที่ต้องคืน</th>
			<th></th>
		</thead>
		<tbody>
			<th scope="row">1</th>
			<td>สามก๊ก</td>
			<td>8/10/60</td>
			<td>15/10/60</td>
			<td><button class="btn btn-success">Renew</button></td>
		</tbody>
		<tbody>
			<th scope="row">2</th>
			<td>Physic</td>
			<td>21/09/60</td>
			<td>28/09/60</td>
			<td><button class="btn btn-success">Renew</button></td>
		</tbody>
	</table>
</div>
<style>
	#Library {
		background-color: white;
		height: 250px;
		border: 1px solid #e1e8ed;
		border-radius: 5px;
	}
</style>