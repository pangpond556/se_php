<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container">
		<form method="post" action="index.php?controller=user&action=edit_profile">
			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Username</label>
				<input type="text" class="form-control" id="username" name="username">
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">Firstname</label>
				<input type="text" class="form-control" id="firstname" name="firstname">
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">Lastname</label>
				<input type="text" class="form-control" id="lastname" name="lastname">
			</div>
			<div class="form-group">
				<label for="faculty" class="col-sm-2 control-label">Faculty</label>
				<input type="text" class="form-control" id="faculty" name="faculty">
			</div>
			<div class="form-group">
				<label for="major" class="col-sm-2 control-label">Major</label>
				<input type="text" class="form-control" id="major" name="major">
			</div>
			<div class="form-group">
				<label for="student_id" class="col-sm-2 control-label">Student_id</label>
				<input type="text" class="form-control" id="student_id" name="student_id">
			</div>
			<div class="form-group">
				<button type="button" value="submit" class="btn btn-default" onclick="this.form.submit();">Submit</button>
			</div>
		</form>
	</div>
</body>
</html>