<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<div class="container">
		<form method="post" action="index.php?controller=news&action=addnews">
			<div class="form-group">
				<label for="title" class="col-sm-2 control-label">Title</label>
				<input type="text" class="form-control" id="title" name="title" required>
			</div>
			<div class="form-group">
				<label for="detail" class="col-sm-2 control-label">Detail</label>
				<textarea class="form-control" rows="3" id="detail" name="detail"></textarea>
			</div>
			<div class="form-group">
				<p><strong>Category</strong></p>
				<select id="category" name="category" class="form-control" required>
					<option value="1">ข่าวประชาสัมพันธ์</option>
					<option value="2">สำหรับนักศึกษา</option>
				</select>
			</div>
			<div class="form-group">
				<button type="button" value="submit" class="btn btn-default" onclick="this.form.submit();">Submit</button>
			</div>
		</form>
	</div>
</body>
</html>