-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 02:49 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uniapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `category` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `detail`, `category`, `img`) VALUES
(5, 'à¸›à¸£à¸°à¸à¸²à¸¨à¸¡à¸«à¸²à¸§à¸´à¸—à¸¢à¸²à¸¥à¸±à¸¢à¹€à¸Šà¸µà¸¢à¸‡à¹ƒà¸«à¸¡à¹ˆ à¹€à¸£à¸·à¹ˆà¸­à¸‡ à¹ƒà¸«à¹‰à¸‡à¸”à¸à¸²à¸£à¹€à¸£à¸µà¸¢à¸™ à¸à¸²à¸£à¸ªà¸­à¸™ à¹à¸¥à¸°à¸à¸²à¸£à¸ªà¸­à¸šà¹ƒà¸™à¸§à¸±à¸™à¸žà¸µà¸˜à¸µà¸žà¸£à¸°à¸£à¸²à¸Šà¸—à¸²à¸™à¸›à¸£à¸´à¸à¸à¸²à¸šà¸±à¸•à¸£ à¸„à¸£à¸±à¹‰à¸‡à¸—à¸µà¹ˆ 52 à¹ƒà¸™à¸§à¸±à¸™à¸­à¸±à¸‡à¸„à¸²à¸£à¸—à¸µà¹ˆ 23 à¸¡à¸à¸£à¸²à¸„à¸¡ 2561', '', 1, NULL),
(6, 'à¹‚à¸­à¸™à¸«à¸™à¹ˆà¸§à¸¢à¸à¸´à¸•/ à¹€à¸—à¸µà¸¢à¸šà¹‚à¸­à¸™à¸«à¸™à¹ˆà¸§à¸¢à¸à¸´à¸•/ à¹€à¸—à¸µà¸¢à¸šà¹‚à¸­à¸™à¸œà¸¥à¸à¸²à¸£à¹€à¸£à¸µà¸¢à¸™à¸™à¸­à¸à¸£à¸°à¸šà¸š(à¸ªà¸³à¸«à¸£à¸±à¸šà¸£à¸°à¸”à¸±à¸šà¸›à¸£à¸´à¸à¸à¸²à¸•à¸£à¸µ)', '', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` text COLLATE utf8_unicode_ci,
  `major` text COLLATE utf8_unicode_ci,
  `student_id` int(11) DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci,
  `cover_img` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `username`, `firstname`, `lastname`, `faculty`, `major`, `student_id`, `img`, `cover_img`) VALUES
(7, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `user_status`) VALUES
(7, 'admin@test.com', 'admin', '$2y$10$nwI8udO35Dray3j3EqYpiOY0CigwEN4yz3MbjxUbpXhp9hW/EE10S', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
