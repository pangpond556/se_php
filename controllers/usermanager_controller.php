<?php
	class UserManagerController
	{
		public $UserManager;
		function __construct() {
			$this->UserManager = new UserManager();
		}
		public function login_na() {
			//$this->UserManager->sec_session_start();
			if(isset($_POST['email'], $_POST['p'])) {
				$email = $_POST['email'];
				$password = $_POST['p'];

				if($this->UserManager->login($email, $password) == true) {
					header('Location: index.php?controller=pages&action=login_success');
				} else {
					header('Location: ../views/pages/error.php?err=Login Failed');
				}
			} else {
				echo "Invalid Request";
			}
		}
		public function logout_na() {
			//$this->UserManager->sec_session_start();
			$_SESSION = array();
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"],
				$params["domain"],
				$params["secure"],
				$params["httponly"]);
			session_destroy();
			header('Location: index.php');
		}
		public function register_na() {
			$this->UserManager->register();
		}
	}
?>