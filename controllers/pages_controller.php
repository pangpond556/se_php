<?php
	class PagesController
	{
		public function home() {
			$hotnews = new News();
			require_once('views/pages/home.php');
		}

		public function newsfeed() {
			require_once('views/pages/newsfeed.php');
		}

		public function profile() {
			require_once('views/pages/profile.php');
		}

		public function library() {
			require_once('views/pages/library.php');
		}

		public function reg() {
			require_once('views/register.php');
		}

		public function addnews() {
			require_once('views/addnews.php');
		}

		public function search() {
			/*if(isset($_POST['search'])){
				$db = Db::getInstance();
			}*/
		}

		public function edit_user() {
			require_once('views/edit_user.php');
		}

		public function login_success() {
			require_once('views/pages/protected_page.php');
		}

		public function reg_success() {
			require_once('views/pages/register_success.php');
		}

		public function addnewssuccess() {
			require_once('views/pages/addnews_success.php');
		}

		public function error() {
			require_once('views/pages/error.php');
		}
	}
?>