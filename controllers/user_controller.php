<?php
	class UserController
	{
		public function profile() {
			$user = new User($_SESSION['username']);
			require_once('views/pages/profile.php');
		}
		public function edit_profile() {
			$user = new User($_SESSION['username']);
			$username;
			$firstname;
			$lastname;
			$faculty;
			$major;
			$student_id;
			if(isset($_POST['username'])){ $username = $_POST['username']; }
			else {$username = $user->getUsername();}

			if(isset($_POST['firstname'])){ $firstname = $_POST['firstname']; }
			else {$firstname = $user->getFirstname();}

			if(isset($_POST['lastname'])){ $lastname = $_POST['lastname']; }
			else {$lastname = $user->getLastname();}

			if(isset($_POST['faculty'])){ $faculty = $_POST['faculty']; }
			else {$faculty = $user->getFaculty();}

			if(isset($_POST['major'])){ $major = $_POST['major']; }
			else {$major = $user->getMajor();}

			if(isset($_POST['student_id'])){ $student_id = $_POST['student_id']; }
			else {$student_id = $user->getStudentID();}

			$user->edit_user($user->getUsername(), $username, $firstname, $lastname, $faculty, $major, $student_id);
		}
		public function edit_profile_pic_na() {
			$user = new User($_SESSION['username']);
			if(isset($_POST['profile_pic'])) {
				$user->edit_profiles_pic();
			} else {
				echo "NO POST Found";
			}
		}
		public function edit_cover_pic_na() {
			$user = new User($_SESSION['username']);
			if(isset($_POST['cover_pic'])) {
				$user->edit_cover_pic();
			} else {
				echo "NO POST Found";
			}
		}
		public function timetable() {
			$user = new User($_SESSION['username']);
			$user->get_timetable(1, 60);
			require_once('views/pages/timetable.php');
		}
	}
?>