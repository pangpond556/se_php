<?php
	class NewsController
	{
		public $n;
		function __construct() {
			$this->n = new News();
		}
		public function newsfeed() {
			require_once('views/pages/newsfeed.php');
		}
		public function viewnews() {
			require_once('views/pages/viewnews.php');
		}
		public function addnews() {
			if(isset($_POST['title'], $_POST['category'])) {
				if(isset($_POST['detail'])){
					$detail = $_POST['detail'];
				} else {
					$detail = NULL;
				}
				$this->n->AddNews($_POST['title'], $_POST['category'], $detail);
			} else {
				echo "Something Wrong $_POST not found";
			}
		}
	}
?>
