<?php
	class BoardController
	{
		public function feed() {
			require_once('views/pages/board.php');
		}

		public function add_board() {
			$b = new Board();
			if(isset($_POST['title'], $_POST['detail'], $_POST['img'], $_POST['tag'])) {
				$b->add($_POST['title'], $_POST['detail'], $_POST['img'], $_POST['tag']);
			} else {
				echo "No POST found";
				exit();
			}
		}
	}
?>