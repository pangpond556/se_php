<?php

	class User
	{
		public $db;
		private $username = NULL;
		private $firstname = NULL;
		private $lastname = NULL;
		private $faculty = NULL;
		private $major = NULL;
		private $student_id = NULL;
		private $img = NULL;
		private $cover_img = NULL;
		public $subject_name = [];
		public $subject_day = [];
		public $subject_time = [];
		public $subject_status = [];
		function __construct($username)
		{
			$this->db = Db::getInstance();
			$stmt = $this->db->prepare("SELECT firstname, lastname, faculty, major, student_id, img, cover_img FROM profiles WHERE `username` = ? LIMIT 1");
			$stmt->bind_param('s' , $username);
			if($stmt->execute()) {
				$stmt->store_result();
				$stmt->bind_result($firstname, $lastname, $faculty, $major, $student_id, $img, $cover_img);
				$stmt->fetch();
				if($stmt->num_rows == 1) {
					$this->username = $username;
					$this->firstname = $firstname;
					$this->lastname = $lastname;
					$this->faculty = $faculty;
					$this->major = $major;
					$this->student_id = $student_id;
					$this->img = $img;
					$this->cover_img = $cover_img;
				} else {
					echo "Something wrong with this user";
					exit();
				}
			} else {
				echo "Database error <br>";
				echo $stmt->error;
				exit();
			}
			
		}
		function getUsername(){ return $this->username; }
		function getFirstname() { return $this->firstname; }
		function getLastname() { return $this->lastname; }
		function getFaculty() { return $this->faculty; }
		function getMajor() { return $this->major; }
		function getStudentID() { return $this->student_id; }
		function getImg(){ return $this->img; }

		function edit_username($oldusername, $username){
			$stmt = $this->db->prepare("UPDATE users SET username = ? WHERE username = ? LIMIT 1");
			$stmt->bind_param('ss', $username, $oldusername);
			$stmt->execute();
		}
		function edit_user($oldusername, $username, $firstname, $lastname, $faculty, $major, $student_id){
			if($stmt = $this->db->prepare("UPDATE profiles SET username = ?, firstname = ?, lastname = ?, faculty = ?, major = ?, student_id = ? WHERE username = ? LIMIT 1")) {
				$stmt->bind_param('sssssis', $username, $firstname, $lastname, $faculty, $major, $student_id, $oldusername);
				if($stmt->execute()){
					$this->edit_username($oldusername, $username);
				} else {
					echo "Database Error";
					exit();
				}
			} else {
				echo "Database Error";
				exit();
			}
		}
		function edit_profiles_pic($img) {
			$stmt = $this->db->prepare("INSERT INTO profiles (img) VALUES (?)");
			$stmt->bind_param('s', $img);
			if($stmt->execute()){
			} else {
				echo $stmt->error;
				exit();
			}
		}
		public function get_timetable($term, $year) {
			if($this->student_id == NULL) {
				echo "Please add your student id or contect admin<br>";
				exit();
			} else {
				$file = file_get_contents('https://www3.reg.cmu.ac.th/regist'.$term.$year.'/public/result.php?id='.$this->student_id );
				$doc = new DOMDocument("1.0", "utf-8");
				@$doc->loadHTML($file);
				$doc->preserveWhiteSpace = false;

				foreach ($doc->getElementsByTagName('tr') as $tr) {
					if($tr->getAttribute('class') == "msan8"){
						$counter = 0;
						foreach ($tr->getElementsByTagName('td') as $td) {
							if($td->getAttribute('bgcolor') == '#E3F1FF'){
								$counter++;
								if($counter == 3){
									array_push($this->subject_name, $td->nodeValue);
								}
								if($counter == 6){
									array_push($this->subject_day, $td->nodeValue);
								}
								if($counter == 7){
									array_push($this->subject_time, $td->nodeValue);
								}
								if($counter == 9){
									array_push($this->subject_status, $td->nodeValue);
								}
							}
						}
					}
				}
			}
		}
	}
?>