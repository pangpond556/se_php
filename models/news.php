<?php
	class News
	{
		public $db;
		public $hotnews = [];
		function __construct()
		{
			$this->db = Db::getInstance();
			$this->HotNews();
		}
		function getFeed() {

		}
		function HotNews() {
			$category_hotnews_id = 1;
			$stmt = $this->db->prepare("SELECT * FROM news WHERE category = ?");
			$stmt->bind_param('i' ,$category_hotnews_id);
			if($stmt->execute()) {
				$result = $stmt->get_result();
				if($result->num_rows >= "1") {
					while($row = $result->fetch_assoc()) {
						array_push($this->hotnews, $row["title"]);
					}
				}
			} else {
			echo "Database Error";
				exit();
			}
		}
		function AddNews($title, $category, $detail) {
			$stmt = $this->db->prepare("INSERT INTO news (title, detail, category) VALUES (?,?,?)");
			$stmt->bind_param('ssi', $title, $detail, $category);
			if($stmt->execute()){
				header('Location: index.php?controller=pages&action=addnewssuccess');
			} else {
				echo "Database Error";
				exit();
			}
		}
	}
?>