<?php
	class Board
	{
		public $db;
		private $title = NULL;
		private $detail = NULL;
		private $img = NULL;
		private $tag = NULL;

		function __construct() {
			$this->db = Db::getInstance();
		}
		function getTitle() {return $this->title;}
		function getDetail() {return $this->detail;}
		function getImg() {return $this->img;}
		function getTag() {return $this->tag;}

		function add($title, $detail, $img, $tag) {
			$stmt = $this->db->prepare("INSERT INTO board (title, detail, img, tag) VALUES (?,?,?,?)");
			$stmt->bind_param('sssi', $title, $detail, $img, $tag);
			if($stmt->execute()) {
			} else {
				echo $stmt->error;
				exit();
			}
		}
	}
?>