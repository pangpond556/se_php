<?php
	function call($controller, $action) {
		require_once('controllers/' .$controller . '_controller.php');

		switch ($controller) {
			case 'pages':
				$controller = new PagesController();
				break;
			case 'usermanager':
				$controller = new UserManagerController();
				break;
			case 'user':
				$controller = new UserController();
				break;
			case 'news':
				$controller = new NewsController();
				break;
			case 'board':
				$controller = new BoardController();
				break;
		}

		$controller->{$action}();
	}

	$controllers = array('pages' => ['home', 'reg', 'library', 'addnews', 'edit_user', 'reg_success', 'addnewssuccess', 'login_success', 'error'],
						'usermanager' => ['login_na', 'logout_na', 'register_na'],
						'user' => ['profile', 'edit_profile', 'timetable'],
						'news' => ['newsfeed', 'viewnews', 'addnews'],
						'board' => ['feed']);

	if(array_key_exists($controller, $controllers)) {
		if(in_array($action, $controllers[$controller])) {
			call($controller, $action);
		} else {
			call('pages', 'error');
		}
	} else {
		call('pages', 'error');
	}
?>
