<?php
	require_once('models/connection.php');
	require_once('models/user_manager.php');
	require_once('models/user.php');
	require_once('models/news.php');
	require_once('models/board.php');
	if(isset($_GET['controller']) && isset($_GET['action'])) {
		$controller = $_GET['controller'];
		$action = $_GET['action'];
	} else {
		$controller = 'pages';
		$action = 'home';
	}

	require_once('views/layout.php');
?>

<style>
	body {
		background-color: #f7f7f7;
	}
</style>